package com.example.kongsun.firebase;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

public class UserAdapter extends ArrayAdapter<User> {

    private Activity context;
    private List<User> userList;

    public UserAdapter(Activity context,List<User> userList){
        super(context,R.layout.item_list_view,userList);
        this.context = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_list_view,null);

        TextView name = view.findViewById(R.id.txt_username);
        TextView pwd = view.findViewById(R.id.txt_password);

        User user = userList.get(position);

        name.setText(user.getUsername());
        pwd.setText(user.getPassword());

        return view;
    }
}
