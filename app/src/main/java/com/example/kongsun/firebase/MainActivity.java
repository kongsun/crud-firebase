package com.example.kongsun.firebase;

import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private ListView listView;
    private List<User> userList;

    private DatabaseReference databaseUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = findViewById(R.id.edit_text_username);
        password = findViewById(R.id.edit_text_password);
        Button btnSave = findViewById(R.id.btn_save);
        listView = findViewById(R.id.list_view_user);
        userList = new ArrayList<>();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseUsers = database.getReference("users");

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addUser();
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                User user = userList.get(position);
                updateUserDialog(user.getUserId(), user.getUsername(), user.getPassword());
                return false;
            }
        });

    }

    private void addUser() {
        String name = username.getText().toString();
        String pwd = password.getText().toString();

        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(pwd)) {

            String id = databaseUsers.push().getKey();
            User user = new User(id, name, pwd);
            databaseUsers.child(id).setValue(user);

            Toast.makeText(this, "User added", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "You should fill in the blank", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        databaseUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    User user = userSnapshot.getValue(User.class);
                    userList.add(user);
                }

                UserAdapter adapter = new UserAdapter(MainActivity.this, userList);
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateUserDialog(final String id, String name, String pwd) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.update_dialog, null);
        dialog.setView(view);

        final EditText edxName = view.findViewById(R.id.edx_username);
        final EditText edxpwd = view.findViewById(R.id.edx_password);
        final Button btnUpdate = view.findViewById(R.id.btn_update);
        final Button btnDelete = view.findViewById(R.id.btn_delete);

        edxName.setText(name);
        edxpwd.setText(pwd);

        dialog.setTitle("Update user :" + name);
        final AlertDialog alertDialog = dialog.create();
        alertDialog.show();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = edxName.getText().toString().trim();
                String pwd = edxpwd.getText().toString().trim();

                if (TextUtils.isEmpty(name) && TextUtils.isEmpty(pwd)) {
                    edxName.setError("Name require");
                    edxpwd.setError("Password require");
                    return;
                }
                updateUser(id, name, pwd);
                alertDialog.dismiss();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteUser(id);
                alertDialog.dismiss();
            }
        });

    }

    private boolean deleteUser(String id) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users").child(id);
        databaseReference.removeValue();
        Toast.makeText(this, "Delete Successful", Toast.LENGTH_SHORT).show();
        return true;
    }

    private boolean updateUser(String id, String username, String pwd) {

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users").child(id);
        User user = new User(id, username, pwd);
        databaseReference.setValue(user);

        Toast.makeText(this, "User updated", Toast.LENGTH_SHORT).show();
        return true;
    }

}
